jQuery('.owl-carousel').owlCarousel({
  loop:true,
  margin:15,
  nav:true,
  dots: true,
  navText :[
  "<img src='css/img/prev.png'>", "<img src='css/img/next.png'>"
  ],
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      },
      1600:{
        items:1
    }
  }
  
})

